### Future releases

### Release v0.0.3-alpha date: 29.10.2018  
Added:
 * configuration file with tax = 0 preset and possibility to change separator 
 * empty blade templates for mini(popup) cart  
 * empty cart check and destroy cart functions  
 * content for mini cart function  
 * all controller methods are now has commented json response in case of React or Vue will be used  
 * /mini_cart route for mini(popup) cart content  
 
  
### Release v0.0.2-alpha date: 19.10.2018
Added:
 * search by id function
 * add, change quantity and delete product functions
 * empty blade template for empty cart  

Changed:
 * Cart helper class, facade and service provider were renamed to "...Wrapper" to prevent namespace collisions

### Release v0.0.1-alpha date: 19.10.2018
Added:
 * basic package structure according to https://bitbucket.org/lenalltd/lenal_standards/wiki/automation_php
 * README.md according to "README.md must have"
 * CHANGELOG.md according to "CHANGELOG.md must have"
 * CONTRIBUTING.md according to "CONTRIBUTING.md must have"