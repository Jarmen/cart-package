{{--
$cart_content - коллекция товаров в корзине
$cart_total - сумма цен товаров в корзине
$item->name - название товара
$item->qty - количество одной позиции товара
$item->id - идентификатор товара для удаления из корзины
$item->options - коллекция с дополнительной информацией
$item->options->image - изображение
$item->options->old_price - старая цена
$item->options->slug - ссылка на страницу карточки
--}}
@foreach ($cart_content as $item)
    {{ $item->name }}
    {{ $item->qty }}
    {{ $item->id }}
    {{ $item->options }}
@endforeach

{{ $cart_total }}