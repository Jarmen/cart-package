<?php
Route::group(['middleware' => ['web']], function () {
    Route::get('/cart', 'lenal\cart\Controllers\CartController@cartContent');
    Route::get('/cart_count', 'lenal\cart\Controllers\CartController@cartCount');

    Route::post('/mini_cart', 'lenal\cart\Controllers\CartController@miniCartContent');
    Route::post('/update_cart', 'lenal\cart\Controllers\CartController@updateCart');
    Route::post('/delete_from_cart', 'lenal\cart\Controllers\CartController@deleteFromCart');
});