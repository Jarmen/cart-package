<?php

namespace lenal\cart\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use lenal\cart\Facades\CartWrapper;
use Illuminate\Http\JsonResponse;

class CartController extends Controller
{
    protected $request;
    protected $response;

    public function __construct(Request $request, JsonResponse $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function cartContent()
    {
        if ($this->cartCount() == 0) {
            return view('cart::cart_empty');
        }

        $cart_content = CartWrapper::getCartContent();
        $cart_total = CartWrapper::getTotal();

        // TODO uncomment when vue components are ready
        // return $response(compact('cart_content', 'cart_total'));

        return view('cart::cart_content', compact('cart_content', 'cart_total'));
    }

    public function miniCartContent()
    {
        if ($this->cartCount() == 0) {
            return view('cart::mini_cart_empty');
        }

        $cart_content = CartWrapper::getCartContent();
        $cart_total = CartWrapper::getTotal();

        // TODO uncomment when vue component is ready
        //return new JsonResponse(compact('cart_total', 'cart_content'));

        return view('cart::mini_cart_content', compact('cart_content', 'cart_total'));
    }

    public function cartCount()
    {
        return CartWrapper::getCartCount();
    }

    public function updateCart()
    {
        CartWrapper::updateCart($this->request);

        if ($this->cartCount() == 0) {
            return view('cart::cart_empty');
        }

        $cart_content = CartWrapper::getCartContent();
        $cart_total = CartWrapper::getTotal();

        // TODO uncomment when vue components are ready
        // return $response(compact('cart_content', 'cart_total'));

        return view('cart::cart_content', compact('cart_content', 'cart_total'));
    }

    public function deleteFromCart()
    {
        CartWrapper::deleteFromCart($this->request);

        if ($this->cartCount() == 0) {
            return view('cart::cart_empty');
        }

        $cart_content = CartWrapper::getCartContent();
        $cart_total = CartWrapper::getTotal();

        // TODO uncomment when vue components are ready
        // return $response(compact('cart_content', 'cart_total'));

        return view('cart::cart_content', compact('cart_content', 'cart_total'));
    }
}