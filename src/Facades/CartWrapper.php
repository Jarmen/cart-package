<?php

namespace lenal\cart\Facades;

use Illuminate\Support\Facades\Facade;

class CartWrapper extends Facade {

    public static function getFacadeAccessor()
    {
        return 'cart_wrapper';
    }
}