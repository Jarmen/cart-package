<?php

namespace lenal\cart;

use Illuminate\Support\ServiceProvider;

class CartServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('cart_wrapper', 'lenal\cart\Helpers\CartWrapper');
    }

    public function boot()
    {
        $this->loadViewsFrom(realpath(dirname(__FILE__)) . '/resources/views', 'cart');
        $this->loadRoutesFrom(realpath(dirname(__FILE__)) . '/routes.php');

        $this->publishes([
            realpath(dirname(__FILE__) . '/config/cart.php') => config_path('cart.php')
        ], 'config');

        $this->publishes([
            base_path('vendor/lenal/cart/') => base_path('packages/lenal/cart')
        ]);
    }
}