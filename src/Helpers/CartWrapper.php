<?php

namespace lenal\cart\Helpers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

// TODO add product models or helpers here
// use app\ProductModel;

class CartWrapper {

    public function getCartContent()
    {
        return Cart::instance('cart')->content();
    }

    public function updateCart(Request $request)
    {
        if ($request->get('action') == 'add') {
            $this->addToCart($request->get('product_id'));
        }

        if ($request->get('action') == 'increase') {
            $found_item = $this->findItem($request->get('product_id'));
            Cart::instance('cart')->update($found_item->rowId, $found_item->qty + 1);
        }

        if ($request->get('action') == 'decrease') {
            $found_item = $this->findItem($request->get('product_id'));
            Cart::instance('cart')->update($found_item->rowId, $found_item->qty - 1);
        }
    }

    public function deleteFromCart(Request $request)
    {
        $product_id = $request->get('product_id');

        $found_item = $this->findItem($product_id);

        if (! empty($found_item)) {
            Cart::instance('cart')->remove($found_item->rowId);
        }
    }

    public function getCartCount()
    {
        return Cart::instance('cart')->count();
    }

    public function getTotal()
    {
        return Cart::instance('cart')->total();
    }

    public function destroyCart()
    {
        Cart::instance('cart')->destroy();
    }

    public function emptyCart()
    {
        if (Cart::instance('cart')->count() == 0) {
            return true;
        }

        return false;
    }

    protected function addToCart($product_id)
    {
        // TODO get product data from Model
        //$product = ProductVariant::find($product_id);

        // TODO add product data to cart
//        Cart::instance('cart')->add([
//            'id' => $product->id,
//            'name' => $product->name,
//            'qty' => 1,
//            'price' => $product->price,
//            'options' => [
//                'image' => url($product->preview_image),
//                'slug' => url('product/' . $product->product()->first()->code)
//            ]
//        ]);
    }

    protected function findItem($product_id)
    {
        return Cart::instance('cart')->search(function($cart_item) use ($product_id) {
            return $cart_item->id == $product_id;
        })->first();
    }
}