## lenal/cart  
  
Cart wrapper for gloudemans/shoppingcart  
  
### Install  
  
  * Add to composer.json  
  
  ```  
  "repositories": [  
          {  
              "type": "git",  
              "url":  "git@gitlab.com:Jarmen/cart-package.git"  
          }  
      ]  
  ```   
  
  If prompted, insert login/password or create auth.json in your global composer directory
    
  * Run command  
  
  ```
  composer require lenal/cart  
  ```  
  
  ### Basic usage  
  * Run command  
  
  ```
  php artisan vendor:publish --provider=lenal\cart\CartServiceProvider --tag=config
  ```  
  
  to copy configuration file into project  
      
  * Run command    
  
  ```
    php artisan migrate
   ```  
    
  ### Package customization  
  
  * Run command  
  for laravel <= 5.4 
  
  ```
    php artisan vendor:publish --provider=lenal\cart\CartServiceProvider
  ```  
  
   for laravel >= 5.5 
    
    ```
      php artisan vendor:publish 
    ```  
   
   select package from list  

All files wiil be copied to packages/lenal/cart
  
  * Register package namespace in composer.json  
  
  ```
    #!json
    "autoload": {
            "psr-4": {
                "lenal\\cart\\": "packages/lenal/cart/src"
            }
        },
   ```   
   
   * Add service provider to 'providers' section in  config/app.php  
   
   ```  
   /*  
    * Package Service Providers...  
    */  
    lenal\cart\CartServiceProvider::class,  
   ```  
   
   * Run command    
   
   ```
   composer dumpautoload
   ```
   
  To prevent namespace collision remove package from vendor
   
   ```
   composer remove lenal/cart
   ```  

   
   ### Use cases and mockups
   https://gitlab.com/Jarmen/cart-package/wikis/home
  